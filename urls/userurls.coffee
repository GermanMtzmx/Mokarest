express = require 'express'
usercontroller = require '../controllers/userController'
permissions = require '../auth/permissions'

# instance your controllers here
signinController = new usercontroller.SigninAPI
meController = new usercontroller.UserAPI

#permissions
permission_classes = new permissions

# user urls
userUrls = express.Router()

userUrls.post "/signin",signinController.signin
userUrls.post "/signup",signinController.signup

userUrls.route "/profile"
	.all(permission_classes.TokenAuthentication)
	.get(meController.profile)
	.put(meController.updateprofile)






module.exports = userUrls

