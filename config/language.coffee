i18nlang = require 'i18n'

i18nlang.configure
	locales: ['en','es']
	directory:__dirname + '/../locales'
	register:global

module.exports = i18nlang
