transport = require '../config/mailingsettings'

sendmail = (from,to,subject,htmlMode,content)->

	email = 
		from:from
		to:to
		subject:subject

	if  htmlMode
		email.html = content
	else
		email.text = content
		
	transport.sendMail(email,(err,info)->

		if err
			return console.log "error trying to send email",err
		return console.log "email sended ... ",info
	)


module.exports = sendmail