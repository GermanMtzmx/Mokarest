jwt = require 'jsonwebtoken'
cfg = require '../config/auth_config'
models = require '../models/'
crypt = require 'bcrypt-nodejs'


authenticate = (data)->
	#console.log data	
	return models.Users.findOne(
		where:
			username:data.username).then (user)->

		if not user
			return null 

		password = crypt.compareSync(data.password,user.password)
		
		if not password
			return null

		key = jwt.sign({id:user.dataValues.uuid},cfg.jwtSecret,{expiresIn:cfg.expires})

		return key
		
module.exports = authenticate
