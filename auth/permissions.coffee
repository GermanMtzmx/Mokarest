jwt = require 'jsonwebtoken'
cfg = require '../config/auth_config'
status = require '../utils/status'

class Permissions

	AllowAny: (req,res,next)->

		next()


	TokenAuthentication: (req,res,next)->

		token = req.headers.authorization
		
		if token 
			jwt.verify token, cfg.jwtSecret, (err,decoded)->
				if err
					return res.status(status.HTTP_401_UNAUTHORIZED).send({'error':'unauthorized'})

				else
					req.user = decoded						
					next()
		else			
			return res.status(status.HTTP_401_UNAUTHORIZED).send({'error':'unauthorized'})

			

	AclPermissions: (dict)->

		(req,res,next)->
			
			console.log "all methods", '*' in dict[req.user.role]

			if not dict[req.user.role]?
				console.log "user is not in dict"

				return res.status(status.HTTP_401_UNAUTHORIZED).send('Your role is not allowed')

			if '*' not in dict[req.user.role] and  req.method not in dict[req.user.role]

				console.log "* not in role key permissions"

				return res.status(status.HTTP_401_UNAUTHORIZED).send('Method not allowed for your role')					

			next()		

		

module.exports = Permissions