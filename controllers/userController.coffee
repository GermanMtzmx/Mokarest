models = require '../models/'
status = require '../utils/status'
authenticate = require '../auth/auth'
sendmail = require '../utils/mailer'


#define clases here 

class SigninAPIController

	signin: (req,res)->

		if not req.body.username
			return res.status(status.HTTP_400_BAD_REQUEST).send(__('username field is required'))
			
		if not req.body.password
			return res.status(status.HTTP_400_BAD_REQUEST).send(__('password field is required'))


		authenticate(req.body).then (key)->

			if key != null			
				data = 
					token: key
				return res.status(status.HTTP_200_OK).send(data)

			return res.status(status.HTTP_400_BAD_REQUEST).send(__('Invalid user or wrong password'))

		.catch (err)->
			console.log err
			return res.status(status.HTTP_400_BAD_REQUEST).send(__("error trying to make auth"))


	signup: (req,res)->
				
		models.Users.create(req.body).then (user)->				
			return res.status(status.HTTP_201_CREATED).send(user.dataValues)
		, (err)->

			return res.status(status.HTTP_400_BAD_REQUEST).send(__("Error trying to signup"))


class UserAPIController

	profile:(req,res)->
		#console.log "request ",req
		return res.status(status.HTTP_200_OK).send(req.user)

	updateprofile:(req,res)->
		return res.status(status.HTTP_200_OK).send(__("profile updated"))
			




#Expor all classes
module.exports =
	SigninAPI:SigninAPIController
	UserAPI:UserAPIController


